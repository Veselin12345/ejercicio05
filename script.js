/*
  Ejercicio: Uso de PAAS para alojar Node.js
  Autor: Veselin Georgiev | veselingp@hotmail.com
  Descripción: Programa que lee un archivo csv, crea una estructura JSON con los datos del archivo y la presenta en una etiqueta HTML específica
*/
function leerArchivoCSV(){
	// Leemos archivo csv
	var archivo = new XMLHttpRequest();
	archivo.open('GET', 'csv.txt', false);
	archivo.onreadystatechange = function(){
		// Verificamos archivo csv
		if(archivo.readyState === 4 && (archivo.status === 200 || archivo.status == 0)){
			var csv = archivo.responseText;
			// Creamos estructura JSON con datos de archivo csv
			json = crearJSON(csv);
			// Presentamos estructura JSON
			document.getElementById('json').innerHTML = 'Estructura JSON: ' + JSON.stringify(json);
		}
	}
	archivo.send(null);
}
function crearJSON(csv){
	// Creamos estructura JSON con datos de archivo csv 
	var json = [];
	var lineas = csv.split('\n');
	var titulos = lineas[0].split(",");
	// Obtenemos datos de archivo csv
	for(i in lineas){
		if(i > 0){
			// Creamos elemento para estructura JSON
			var elemento = {};
			var linea = lineas[i].split(",");
			for(j in titulos)
				elemento[titulos[j]] = linea[j];
			// Agregamos elemento a estructura JSON
			json.push(elemento);
		}
	}
	return json;
}