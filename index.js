/*
  Ejercicio: Uso de PAAS para alojar Node.js
  Autor: Veselin Georgiev | veselingp@hotmail.com
  Descripción: Programa que lee un archivo csv, crea una estructura JSON con los datos del archivo y la presenta en una etiqueta HTML específica
*/
// Inicializamos módulos a utilizar
var http = require('http');
var fs = require('fs');
var path = require('path');
var IP = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var PORT = process.env.OPENSHIFT_NODEJS_PORT || 8080;
// Creamos servidor HTTP
http.createServer(function(peticion, respuesta){
	// Obtenemos nombre de archivo solicitado
	var nombreArchivo = '.' + peticion.url;
	console.log('Archivo solicitado: ' + nombreArchivo); 
	if(nombreArchivo == './')
		nombreArchivo = './index.html';
	// Obtenemos extensión de archivo solicitado
	var extensionArchivo = path.extname(nombreArchivo);
	var contentType = 'text/html';
	switch(extensionArchivo){
		case '.js':
			contentType = 'text/javascript';
			break;
		case '.html':
			contentType = 'text/html';
			break;
		default:
			contentType = 'text/plain';
			break;
	}
	fs.readFile(nombreArchivo, function(error, archivo){
		if(error){
			if(error.code == 'ENOENT'){
				fs.readFile('./404.html', function(error, archivo){
					respuesta.writeHead(200, {'Content-Type': contentType});
					respuesta.end(archivo, 'utf-8');
				});
			}
		}
		else{
			respuesta.writeHead(200, {'Content-Type': contentType});
			respuesta.end(archivo, 'utf-8');
		}
	});
}).listen(PORT, IP);
console.log('Servidor ejecutándose en: http://' + IP + ':' + PORT + '/');